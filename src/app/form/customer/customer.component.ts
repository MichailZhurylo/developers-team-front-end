import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm, NgModel} from '@angular/forms';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  @ViewChild('SomeReference') form: NgForm;
  @ViewChild('name') modelName: NgModel;

  somevalues = ['Value 1', 'Value 2'];
  formSubmitted;
  user = {
    name: '',
    address: ''
    // ,somevalue: ''
  };

  constructor() {
  }

  ngOnInit() {
  }

  // readForm(form: NgForm) {
  readForm() {
    // console.log(this.form);
    // console.log(this.modelName);
    this.user.name = this.form.value.name;
    this.user.address = this.form.value.address;
    // this.user.somevalue = this.form.value.somevalue;
    console.log(this.user);
    this.formSubmitted = true;
  }

  /*
  console.log(form.value.name);
  console.log(form.value.address);
  console.log(form.value.phone);
*/
  onRowClick() {

  }

  clearForm(form: NgForm) {
    form.onReset();
  }
}
